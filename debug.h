// This code (cc) 2015 by Soleil Lapierre and released under the CC BY-SA 4.0 license.
// License terms here: http://creativecommons.org/licenses/by-sa/4.0/

#ifndef _LCDDEBUG_H_
#define _LCDDEBUG_H_

// Enabling USB debugging requires usb_debug_only.h/.c and print.h/.c which are
// available in the code library at http://www.pjrc.com/teensy/

// Note: Be careful about using the debug macros too liberally. I've found they
// can easily, accidentlally corrupt program operation by interfering with
// critical timing.

#ifndef DEBUG_MODE
//#define DEBUG_MODE // Uncomment or define in makefile to enable USB debugging.
#endif

#ifdef DEBUG_MODE

#include "usb_debug_only.h"
#include "print.h"

#define DEBUG_INIT() usb_init()
#define DEBUG_PRINT(s) print(s)
#define DEBUG_PUTCHAR(c) pchar(c)
#define DEBUG_PRINTHEX(c) phex16((unsigned int)c)
#define DEBUG_FLUSH() usb_debug_flush_output()

#else

#define DEBUG_INIT()
#define DEBUG_PRINT(s)
#define DEBUG_PUTCHAR(c)
#define DEBUG_PRINTHEX(c)
#define DEBUG_FLUSH()

#endif

#endif
