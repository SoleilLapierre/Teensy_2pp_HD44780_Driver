// This code (cc) 2015 by Soleil Lapierre and released under the CC BY-SA 4.0 license.
// License terms here: http://creativecommons.org/licenses/by-sa/4.0/

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "Teensy_HD44780.h"
#include "debug.h"

// Local constants for tracking LCD internal state (command register bits)
#define LCDCMD_CLEAR	0x01  // Clear display command.
#define LCDCMD_HOME		0x02  // Home cursor command.
#define LCDCMD_EMSET	0x04  // Entry mode set command.
#define LCDMODE_ID		0x02  // Entry mode: I/D (Incrememt/Decrement cursor)
#define LCDMODE_S       0x01  // Entry mode: S (display shifts with cursor)
#define LCDCMD_DONOFF   0x08  // Display on/off command.
#define LCDMODE_D       0x04  // Turn display on.
#define LCDMODE_C       0x02  // Turn cursor on.
#define LCDMODE_B       0x01  // Turn cursor blink on.
#define LCDCMD_SHIFT    0x10  // Cursor/display shift command.
#define LCDMODE_SC      0x08  // Shift mode: S/C (screen/cursor shifts).
#define LCDMODE_RL      0x04  // Shift mode: R/L (left/right shift).
#define LCDCMD_FUNCSET  0x20  // Function setup command.
#define LCDMODE_DL      0x10  // Setup: Data length (DL) = 8 bits.
#define LCDMODE_N       0x08  // Setup: Number of display rows (N).
#define LCDMODE_F       0x04  // Setup: Font selection (F).
#define LCDCMD_SETCGRA  0x40  // Set character generator RAM address command.
#define LCDMODE_CGRA_MASK 0x3F // Mask for character generator RAM addresses.
#define LCDCMD_SETDDRA  0x80  // Set DDRAM address command.
#define LCDMODE_BF      0x80  // Busy flag read bit.
#define LCDMODE_DDRA_MASK 0x7F // Mask for DDRAM addresses.

// Arguments for LCDWrite() and LCDRead().
#define LCD_COMMAND 1         
#define LCD_DATA    0

// Internal state tracking.
int gLCDError = 1;
unsigned char gEntryModeReg;	  // Tracks state of LCD controller's entry mode register (I/D and S)
unsigned char gDisplayControlReg; // Tracks state of LCD controller's display control register (D, C and B)
// We don't track the state of the function set register (DL, N and F) because this library only
// supports setting them up and init time via compile-time constants, because the hardware
// is assumed not to change during runtime.
// The Shift command (S/C and R/L is an effect, not a state register, so doesn't need to be tracked.


// Private functions.
void LCDWrite(char aIsCommand, unsigned char aData);
unsigned char LCDRead(char aIsCommand);
void LCDBusyWait(void);
void LCDLatchCommand(void);


void LCDInit(void)
{
	gLCDError = 0;

	// Set all output lines low to prevent accidental latch.
	DEBUG_PRINT("LCDInit: Initializing output lines\n"); DEBUG_FLUSH();
	TEENSY_HD44780_DDR &= TEENSY_HD44780_UNUSED_BITS;			// Set all 7 pins to input mode so we can...
	TEENSY_HD44780_PORT_WRITE &= TEENSY_HD44780_UNUSED_BITS;	// Turn off pullup resistors for upper 3 output bits.
	TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_DATA_BITS;		// Turn on pullup resistors for lower 4 input bits.
	TEENSY_HD44780_DDR |= TEENSY_HD44780_ALL_BITS;				// Set all pins to output. (Bit 8 is always ignored so other programs can use it).
	TEENSY_HD44780_PORT_WRITE &= TEENSY_HD44780_UNUSED_BITS;	// Set all outputs low.

	// The first command has to be done the long way because it sets up
    // the four-bit transfer mode.  All other code assumes 4-bit.

	DEBUG_PRINT("LCDInit: Setting 4-bit mode\n"); DEBUG_FLUSH();
    LCDBusyWait();												// Wait for hardware initialization of the LCD to finish.
	TEENSY_HD44780_DDR |= TEENSY_HD44780_ALL_BITS;				// Set all pins to output.
    TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_RS;			// Select command register.
    TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_RW;			// Write mode.
	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_DATA_BITS;		// Clear data lines.
	TEENSY_HD44780_PORT_WRITE |= (LCDCMD_FUNCSET >> 4);			// Set 4-bit transfer mode (DL = 1).
	// The above is right shifted by 4 because we're initially in 8-bit transfer mode
	// and the lower 4 bits of our output register are assumed to be wired to the
	// upper 4 bits of the LCD's I/O header. After we initialize 4-bit mode this
	// shift is no longer needed.

    LCDLatchCommand(); // Latch first command nybble (this counts as an 8-bit command)
    LCDLatchCommand(); // Do it again now that we're in 4-bit mode.

    // Set up low word of command.
	DEBUG_PRINT("LCDInit: Setting display mode\n"); DEBUG_FLUSH();
	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_DATA_BITS;
#if (TEENSY_HD44780_DISPLAY_ROWS == 2)
	TEENSY_HD44780_PORT_WRITE |= LCDMODE_N;  // 2-line display mode (N = 1).
#endif

#if (TEENSY_HD44780_TALL_FONT != 0)
	TEENSY_HD44780_PORT_WRITE |= LCDMODE_F; // 5x10 dot font (F = 1).
#endif

    LCDLatchCommand(); // Latch second command nybble.
    LCDBusyWait();

    if (LCDGetStatus() != 0)
    {
		DEBUG_PRINT("LCDInit: Error detected\n"); DEBUG_FLUSH();
        return;
    }

    // Now we can use the simpler API to do the rest of the initialization.
	DEBUG_PRINT("LCDInit: Enabling display.\n"); DEBUG_FLUSH();
	gDisplayControlReg = LCDMODE_D | LCDMODE_C | LCDMODE_B;
	LCDWrite(LCD_COMMAND, LCDCMD_DONOFF | gDisplayControlReg); // Enable display, disable cursor and blink (D = C = B = 1).
	DEBUG_PRINT("LCDInit: Setting cursor mode.\n"); DEBUG_FLUSH();
	gEntryModeReg = LCDMODE_ID;
	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | gEntryModeReg); // Set cursor auto-increment without display shift (I/D = 1, S = 0).
	DEBUG_PRINT("LCDInit: Clearing display.\n"); DEBUG_FLUSH();
	LCDWrite(LCD_COMMAND, LCDCMD_CLEAR); // Clear display contents and home cursor.
}


int LCDGetStatus(void)
{
    return gLCDError;
}


void LCDWriteString(const char* aMsg)
{
	const char* p = aMsg;
	if (p == 0)
    {
        return; 
    }

    while ((*p != '\0') && (LCDGetStatus() == 0))
    {
		LCDWrite(LCD_DATA, (unsigned char)(*p));
        ++p;
    }
}


void LCDWriteChar(unsigned char aCode)
{
	LCDWrite(LCD_DATA, aCode);
}


void LCDWriteChars(unsigned char *aMsg, int aLength)
{
	if (aMsg == 0)
	{
		return;
	}

	int i;
	for (i = 0; i < aLength; ++i)
	{
		LCDWrite(LCD_DATA, aMsg[i]);
	}
}


unsigned char LCDReadChar(void)
{
	unsigned char result = LCDRead(LCD_DATA);
	LCDBusyWait(); // Address counters in the controller don't update until BF = 0.
	return result;
}



void LCDWrite(char aIsCommand, unsigned char aData)
{
    if (LCDGetStatus() != 0)
    {
		DEBUG_PRINT("LCDWrite: Bailing due to pre-existing error state.\n"); DEBUG_FLUSH();
        return;
    }

    TEENSY_HD44780_DDR |= TEENSY_HD44780_ALL_BITS; // All outputs.

    if (aIsCommand == 0)
    {
        TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_RS; // Data register.
    }
    else
    {
        TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_RS; // Command register.
    }

    TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_RW; // Write mode.

	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_DATA_BITS; // Clear data bits;
	TEENSY_HD44780_PORT_WRITE |= (aData >> 4) & TEENSY_HD44780_DATA_BITS; // First nybble.
    LCDLatchCommand();

	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_DATA_BITS; // Clear data bits;
	TEENSY_HD44780_PORT_WRITE |= aData & TEENSY_HD44780_DATA_BITS; // Second nybble.
    LCDLatchCommand();
    LCDBusyWait();
}


unsigned char LCDRead(char aIsCommand)
{
	unsigned char result = 0;

	TEENSY_HD44780_DDR |= TEENSY_HD44780_CONTROL_BITS;
	TEENSY_HD44780_DDR &= ~TEENSY_HD44780_DATA_BITS; // Data lines are input.
	if (aIsCommand != 0)
	{
		TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_RS; // Select command register.
		TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_RW;  // Read mode.
	}
	else
	{
		TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_RS | TEENSY_HD44780_RW;  // Select data register and read mode.
	}

	// Data is only valid while E is high, which is why we don't call LCDLatchCommand() here.
	// Read high nybble.
	_delay_us(1); // LCD controller only requires 40nS between RS/RW setup and E, but we don't have a timer that fine.
	TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_E;
	_delay_us(1);
	result = (TEENSY_HD44780_PORT_READ & TEENSY_HD44780_DATA_BITS) << 4;
	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_E;
	_delay_us(1);

	// Read low nybble.
	TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_E;
	_delay_us(1);
	result |= (TEENSY_HD44780_PORT_READ & TEENSY_HD44780_DATA_BITS);
	TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_E;
	_delay_us(1);

	return result;
}



void LCDBusyWait(void)
{
    int count = 0;
	int result = 0;

    if (LCDGetStatus() != 0)
    {
		DEBUG_PRINT("LCDBusyWait bailing due to pre-existing error state."); DEBUG_FLUSH();
        return;
    }

	result = LCDRead(LCD_COMMAND);
	while ((result & LCDMODE_BF) != 0)
    {
		++count;
		if (count > 30000) // No LCD operation should take more than 50mS.
		{
			DEBUG_PRINT("LCDBusyWait: TIMEOUT\n"); DEBUG_FLUSH();
			gLCDError = 1;
			break;
		}

		result = LCDRead(LCD_COMMAND);
    }  
}


void LCDLatchCommand(void)
{
  _delay_us(1);
  TEENSY_HD44780_PORT_WRITE |= TEENSY_HD44780_E;
  _delay_us(1);
  TEENSY_HD44780_PORT_WRITE &= ~TEENSY_HD44780_E;
  _delay_us(1);
}


void LCDClear(void)
{
	LCDWrite(LCD_COMMAND, LCDCMD_CLEAR);
}


void LCDCursorHome(void)
{
	LCDWrite(LCD_COMMAND, LCDCMD_HOME);
}


void LCDEnableDisplay(char aEnable)
{
	if (aEnable != 0)
	{
		gDisplayControlReg |= LCDMODE_D;
	}
	else
	{
		gDisplayControlReg &= ~LCDMODE_D;
	}

	LCDWrite(LCD_COMMAND, LCDCMD_DONOFF | gDisplayControlReg);
}


void LCDEnableCursor(char aEnable)
{
	if (aEnable != 0)
	{
		gDisplayControlReg |= LCDMODE_C;
	}
	else
	{
		gDisplayControlReg &= ~LCDMODE_C;
	}

	LCDWrite(LCD_COMMAND, LCDCMD_DONOFF | gDisplayControlReg);
}


void LCDEnableCursorBlink(char aEnable)
{
	if (aEnable != 0)
	{
		gDisplayControlReg |= LCDMODE_B;
	}
	else
	{
		gDisplayControlReg &= ~LCDMODE_B;
	}

	LCDWrite(LCD_COMMAND, LCDCMD_DONOFF | gDisplayControlReg);
}


void LCDSetCursorDirection(char aIncrement)
{
	if (aIncrement)
	{
		gEntryModeReg |= LCDMODE_ID;
	}
	else
	{
		gEntryModeReg &= ~LCDMODE_ID;
	}

	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | gEntryModeReg);
}


void LCDSetDisplayShiftEnable(char aEnable)
{
	if (aEnable)
	{
		gEntryModeReg |= LCDMODE_S;
	}
	else
	{
		gEntryModeReg &= ~LCDMODE_S;
	}

	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | gEntryModeReg);
}


void LCDSetCursorAddress(int aAddress)
{
	LCDWrite(LCD_COMMAND, LCDCMD_SETDDRA | (unsigned char)(aAddress & LCDMODE_DDRA_MASK));
}


void LCDMoveCursorRelative(int aLeft, int aSpaces)
{
	int i;

	unsigned char command = LCDCMD_SHIFT;
	if (aLeft != 1)
	{
		command |= LCDMODE_RL;
	}

	for (i = 0; i < aSpaces; ++i)
	{
		LCDWrite(LCD_COMMAND, command);
	}
}


void LCDMoveScreenRelative(int aLeft, int aSpaces)
{
	int i;

	unsigned char command = LCDCMD_SHIFT | LCDMODE_SC;
	if (aLeft != 0)
	{
		command |= LCDMODE_RL;
	}

	for (i = 0; i < aSpaces; ++i)
	{
		LCDWrite(LCD_COMMAND, command);
	}
}


// The following code is for interfacing with the LCD's character generation
// and can be disabled at compile time if you're not using it.
#if (TEENSY_HD44780_ENABLE_CHARGEN_ACCESS == 1)

void LCDWriteCharGenData(unsigned char aCode, unsigned char* aData, int aCharCount)
{
	// Force increment mode, as it also affects character generator RAM access.
	unsigned char mode = gEntryModeReg;
	unsigned char address = 0;
	int i;

	mode |= LCDMODE_ID;
	mode &= ~LCDMODE_S;
	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | mode);

	// Set the CG address to initiate data transfer.
#if (TEENSY_HD44780_TALL_FONT == 0)
	address = (aCode << 3) & LCDMODE_CGRA_MASK;
#else
	address = ((aCode & 0x06) << 3) & LCDMODE_CGRA_MASK;
#endif
	LCDWrite(LCD_COMMAND, LCDCMD_SETCGRA | address);

	// Transfer data.
#if (TEENSY_HD44780_TALL_FONT == 0)
	for (i = 0; i < 8 * aCharCount; ++i)
#else
	for (i = 0; i < 16 * aCharCount; ++i)
#endif
	{
		LCDWrite(LCD_DATA, aData[i]);
	}

	// Restore original entry mode.
	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | gEntryModeReg);
}


void LCDReadCharGenData(unsigned char aCode, unsigned char* aOutput, int aCharCount)
{
	// Force increment mode, as it also affects character generator RAM access.
	unsigned char mode = gEntryModeReg;
	unsigned char address = 0;
	int i;

	mode |= LCDMODE_ID;
	mode &= ~LCDMODE_S;
	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | mode);

	// Set the CG address to initiate data transfer.
#if (TEENSY_HD44780_TALL_FONT == 0)
	address = (aCode << 3) & LCDMODE_CGRA_MASK;
#else
	address = ((aCode & 0x06) << 3) & LCDMODE_CGRA_MASK;
#endif
	LCDWrite(LCD_COMMAND, LCDCMD_SETCGRA | address);

	// Transfer data.
#if (TEENSY_HD44780_TALL_FONT == 0)
	for (i = 0; i < 8 * aCharCount; ++i)
#else
	for (i = 0; i < 16 * aCharCount; ++i)
#endif
	{
		aOutput[i] = LCDRead(LCD_DATA);
		LCDBusyWait();
	}

	// Restore original entry mode.
	LCDWrite(LCD_COMMAND, LCDCMD_EMSET | gEntryModeReg);
}


void LCDDefineUserChar(unsigned char aCode, unsigned char* aData)
{
	LCDWriteCharGenData(aCode, aData, 1);
}


void LCDReadUserChar(unsigned char aCode, unsigned char* aOutput)
{
	LCDReadCharGenData(aCode, aOutput, 1);
}


void LCDDefineAllUserChars(unsigned char* aData)
{
#if (TEENSY_HD44780_TALL_FONT == 0)
	LCDWriteCharGenData(0, aData, 8);
#else
	LCDWriteCharGenData(0, aData, 4);
#endif
}


void LCDReadAllUserChars(unsigned char* aOutput)
{
#if (TEENSY_HD44780_TALL_FONT == 0)
	LCDReadCharGenData(0, aOutput, 8);
#else
	LCDReadCharGenData(0, aOutput, 4);
#endif
}

#endif // CHARGEN_ACCESS


// Clean up: Undefine local constants.
#undef LCDCMD_CLEAR
#undef LCDCMD_HOME
#undef LCDCMD_EMSET
#undef LCDMODE_ID
#undef LCDMODE_S
#undef LCDCMD_DONOFF
#undef LCDMODE_D
#undef LCDMODE_C
#undef LCDMODE_B
#undef LCDCMD_SHIFT
#undef LCDMODE_SC
#undef LCDMODE_RL
#undef LCDCMD_FUNCSET
#undef LCDMODE_DL
#undef LCDMODE_N
#undef LCDMODE_F
#undef LCDCMD_SETCGRA
#undef LCDMODE_CGRA_MASK
#undef LCDCMD_SETDDRA
#undef LCDMODE_DDRA_MASK
#undef LCDMODE_BF
#undef LCDMODE_AC_MASK
#undef LCD_COMMAND
#undef LCD_DATA
