This is a support library I wrote to drive a Hitachi HD44780-based LCD module for my robotics projects. 

It works on the Teensy 2++ microcontroller breakout module and has not been tested on any others.

Includes a Hello World example program. See the code for information on how to adjust
it to use your particular interface wiring. It needs seven bidirectional I/O lines - four
for data and three for control signals. The four data lines must be contiguous and on the
same port but the control signals can be scattered around or on different ports.

This isn't properly library-ized yet (ie doesn't build as a .lib).

The original project writeup and a demo video are here: 
http://www.soleillapierre.ca/projects/software/teensy_lcd/

