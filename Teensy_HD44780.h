// This code (cc) 2015 by Soleil Lapierre and released under the CC BY-SA 4.0 license.
// License terms here: http://creativecommons.org/licenses/by-sa/4.0/
//
// Public include file for interfacing a Teensy++ microcontroller breakout board with
// any textual LCD display that uses the very common HD44780 controller chip.
// This code uses the 4-bit transfer mode of the LCD controller so you only need
// 7 I/O lines to interface it instead of 11.
//
// By default, this code assumes the 7 I/O lines are the lower 7 bits of one port,
// and the data lines are the lower 4 bits of those. See the macros below for more
// info. If you need to remix the wiring you may need to alter the code accordingly.
//
// The HD44780 supports displays having one or two lines of text, 40 characters per
// line, and either 5x8 or 5x10 fonts.  It does not support both 5x10 and 2 lines at
// the same time. It has some user-programmable characters - 8 for 5x8 font mode
// or 4 for 5x10 mode.  There are some different versions of the chip that ship
// with different character sets predefined in ROM - see the chip's data sheet for 
// examples.
//
// This code comes with no warranty and no guarantee of accuracy or completeness.
// Use at your own risk. 5x10 font support is untested.

#ifndef _TEENSY_HD44780_H_
#define _TEENSY_HD44780_H_

#include <avr/io.h>

// Tell the code which data direction register and I/O port to use.
// Make sure they match! Can be overridden by compiler command-line defines.
#ifndef TEENSY_HD44780_DDR
#define TEENSY_HD44780_DDR DDRC
#endif

#ifndef TEENSY_HD44780_PORT_WRITE
#define TEENSY_HD44780_PORT_WRITE PORTC
#endif

#ifndef TEENSY_HD44780_PORT_READ
#define TEENSY_HD44780_PORT_READ PINC
#endif

// Change this to 1 if your LCD only has one row of text.
#ifndef TEENSY_HD44780_DISPLAY_ROWS
#define TEENSY_HD44780_DISPLAY_ROWS 2
#endif

// Change this to 1 to use the tall (5x10) character set
// instead of the default 5x8.
// Normally this only works on special 1-row displays.
#ifndef TEENSY_HD44780_TALL_FONT
#define TEENSY_HD44780_TALL_FONT 0
#endif

// Set this to 0 to disable the character generator code in order to
// save space if you're not using it.
#ifndef TEENSY_HD44780_ENABLE_CHARGEN_ACCESS
#define TEENSY_HD44780_ENABLE_CHARGEN_ACCESS 1
#endif

// Note the lower 4 bits of the port are assumed to be connected
// to the high 4 data bus bits of the LCD (this code uses 4-bit
// transfer mode to save I/O pins).

// Which bit is connected to the Register Select line on the LCD?
#ifndef TEENSY_HD44780_RS
#define TEENSY_HD44780_RS 0x10
#endif

// Which bit is connected to the read/write line on the LCD?
#ifndef TEENSY_HD44780_RW
#define TEENSY_HD44780_RW 0x20
#endif

// Which bit is connected to the Enable line on the LCD?
#ifndef TEENSY_HD44780_E
#define TEENSY_HD44780_E 0x40
#endif

#define TEENSY_HD44780_CONTROL_BITS (TEENSY_HD44780_RS | TEENSY_HD44780_RW | TEENSY_HD44780_E)

// Mask for the four bits used for LCD communitcation.
// NOTE: The code assumes these are four contiguous bits in the lower
// nybble of the port! If you need to rewire them, you will need to
// rewrite code that uses this constant to transfer data to and from the LCD.
#ifndef TEENSY_HD44780_DATA_BITS
#define TEENSY_HD44780_DATA_BITS 0x0F
#endif

#define TEENSY_HD44780_ALL_BITS (TEENSY_HD44780_CONTROL_BITS | TEENSY_HD44780_DATA_BITS)
#define TEENSY_HD44780_UNUSED_BITS (~TEENSY_HD44780_ALL_BITS)

// Call on system start to initialize the LCD I/O and clear the display.
// May need a delay of 20mS or so following poweron before calling this,
// to give the LCD controller time to initialize itself.
void LCDInit(void);

// Returns nonzero on error;
int LCDGetStatus(void);

// Output a NULL-terminated string at the current cursor position. 
// Does not interpret CR/LF or other control chars.
// Note that 0 is one of the user-programmable chars, and can't
// be output this way.
void LCDWriteString(const char* aMsg);

// Write any characters (including 0) at the current cursor position (and move cursor).
void LCDWriteChar(unsigned char aCode);
void LCDWriteChars(unsigned char *aMsg, int aLength); // aMsg does not have to be NULL-terminated.

// Read back the character at the current cursor position (also moves cursor).
// Result is undefined in an error state.
unsigned char LCDReadChar(void);

// Clear the display or home the cursor.
void LCDClear(void);
void LCDCursorHome(void);

// Enable/disable stuff.
void LCDEnableDisplay(char aEnable);
void LCDEnableCursor(char aEnable);
void LCDEnableCursorBlink(char aEnable);

// Scrolling and cursor positioning stuff.
void LCDSetCursorDirection(char aIncrement); // 1 = increment (default), 0 = decrement on read/write.
void LCDSetDisplayShiftEnable(char aEnable); // 1 = enable display shift with cursor movement.
void LCDSetCursorAddress(int aAddress);      // Jump cursor to specific address (0-127).
#define TEENSY_HD44780_LINE2_START 0x40		 // Constant for the cursor address of row 2 first character.
void LCDMoveCursorRelative(int aLeft, int aSpaces);
void LCDMoveScreenRelative(int aLeft, int aSpaces);

// Character generator stuff.
// Results are undefined in an error state.
#if (TEENSY_HD44780_ENABLE_CHARGEN_ACCESS == 1)

// Set character generator data for one user character.
// With the 5x8 font mode, aCode can be 0 through 7 
// and aData must point to 8 bytes.
// With the 5x10 font mode, aCode can be 0 through 7 
// but the LSB is ignored (only 4 chars actually available)
// and aData must point to 16 bytes (note the last 5 are ignored 
// and can be used for data storage).
// In both cases, only the 5 LSBs of the data are used for display 
// and the upper 3 bits can be used for storage.
void LCDDefineUserChar(unsigned char aCode, unsigned char* aData);

// Can be used to read back data stored with LCDDefineUserChar.
// Same rules apply, especially about the output buffer size.
void LCDReadUserChar(unsigned char aCode, unsigned char* aOutput);

// Define all user chars at once. 
// aData must point to 64 bytes.
// In 5x8 font mode, this will define 8 characters (8 bytes each).
// In 5x10 font mode, this will define 4 characters, each starting
// at a 16-byte boundary.  The upper 5 bytes of each are unused and
// can be used as external memory, as can the upper 3 bits of the
// actually used bytes.
void LCDDefineAllUserChars(unsigned char* aData);

// Read back all user character definitions.
// aOutput must point to a buffer of at least 64 bytes.
void LCDReadAllUserChars(unsigned char* aOutput);

#endif


#endif // Include guard.
