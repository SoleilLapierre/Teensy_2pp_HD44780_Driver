// This code (cc) 2015 by Soleil Lapierre and released under the CC BY-SA 4.0 license.
// License terms here: http://creativecommons.org/licenses/by-sa/4.0/

// Based on example source from the code library at http://www.pjrc.com/teensy/
// This is a demo program for the Teensy_HD44780 library by Soleil Lapierre.
// It will flash the LED if an error is detected interfacing with the LCD;
// otherwise it will cycle between a few different demos.
// Note this demo assumes 2-line, 5x8 font mode.

#include "Teensy_HD44780.h"
#include "debug.h"
#include <util/delay.h>

// Teensy 2.0: LED is active high
#if defined(__AVR_ATmega32U4__) || defined(__AVR_AT90USB1286__)
#define LED_ON		(PORTD |= (1<<6))
#define LED_OFF		(PORTD &= ~(1<<6))

// Teensy 1.0: LED is active low
#else
#define LED_ON	(PORTD &= ~(1<<6))
#define LED_OFF	(PORTD |= (1<<6))
#endif

#define LED_CONFIG	(DDRD |= (1<<6))
#define CPU_PRESCALE(n)	(CLKPR = 0x80, CLKPR = (n))

#include "lcd_chardata.c"

unsigned char gAnimData[64];

// ------ Demo functions -----
void HelloWorldDemo(void)
{
	LCDClear();
	LCDEnableCursor(1);
	LCDEnableCursorBlink(1);
	LCDSetCursorAddress(6);
	LCDWriteString("Hello,");
	LCDSetCursorAddress(6 + TEENSY_HD44780_LINE2_START);
	LCDWriteString("World!");
	_delay_ms(5000);
}


void ScrollingDemo(void)
{
	int time = 0;

	LCDClear();
	LCDEnableCursor(0);
	LCDEnableCursorBlink(0);
	LCDWriteString("Scrolling");
	LCDSetCursorAddress(TEENSY_HD44780_LINE2_START + 16);
	LCDWriteString("Example");

	_delay_ms(1000);
	do
	{
		LCDMoveScreenRelative(0, 1);
		_delay_ms(200);
		time += 200;
	} while (time < 5000);
	
	do
	{
		LCDMoveScreenRelative(1, 1);
		_delay_ms(200);
		time += 200;
	} while (time < 10000);
}


void CustomCharDemo(void)
{
	int i, j, wave, increment;
	int time = 0;

	LCDClear();
	LCDEnableCursor(0);
	LCDEnableCursorBlink(0);
	LCDWriteString(" Custom Glyphs");
	LCDSetCursorAddress(6 + TEENSY_HD44780_LINE2_START);
	for (i = 0; i < 8; ++i)
	{
		LCDWriteChar((unsigned char)i);
	}

	LCDDefineAllUserChars(gCharData);
	_delay_ms(4000);

	LCDReadAllUserChars(gAnimData);
	wave = 3;
	increment = 1;
	while (time < 10000)
	{
		for (j = 0; j < 8; ++j)
		{
			for (i = 0; i < 8; ++i)
			{
				gAnimData[8 * j + i] <<= 1;
				if (j > 0)
				{
					gAnimData[8 * (j - 1) + i] |= (gAnimData[8 * j + i] >> 5) & 0x01;
				}

				gAnimData[8 * j + i] &= 0x1F;
			}
		}

		gAnimData[8 * 7 + wave] |= 1;
		wave += increment;
		if ((wave == 0) || (wave == 7))
		{
			increment *= -1;
		}

		LCDDefineAllUserChars(gAnimData);

		_delay_ms(200);
		time += 200;
	}
}


void (*gDemoFunctions[])(void) =
{
	HelloWorldDemo,
	ScrollingDemo,
	CustomCharDemo,
	0
};

// ----- MAIN -----
int main(void)
{
	int error = 0;
	int demo = 0;

    // set for 16 MHz clock, and make sure the LED is off
    CPU_PRESCALE(0);
    LED_CONFIG;
    LED_OFF;
	DEBUG_INIT();

// Wait for PC USB initialization.
#ifdef DEBUG_MODE
    _delay_ms(4000);
#else
    _delay_ms(1000);
#endif

	DEBUG_PRINT("----- Initializing LCD.\n"); DEBUG_FLUSH();
	LCDInit();

	DEBUG_PRINT("----- Entering main loop.\n"); DEBUG_FLUSH();
	error = LCDGetStatus();
	while (error == 0)
	{
		// Cycle through the demo functions.
		(*gDemoFunctions[demo])();
		++demo;
		if (gDemoFunctions[demo] == 0)
		{
			demo = 0;
		}

		error = LCDGetStatus();
	}

    // Signal end of program by blinking LCD.
	DEBUG_PRINT("----- Detected an error; flashing LED.\n"); DEBUG_FLUSH();
	while (1)
    {
        LED_ON;
	    _delay_ms(100);
	    LED_OFF;
	    _delay_ms(100);
    }
}
